#include <cstdio>
#include <cstring>
#include <cstdlib>
#include <iostream>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <dirent.h>
#include <ftw.h>

#include <map>
#include <vector>

using namespace std;

char **dir;
struct dirent *entry;
DIR *dp;


static unsigned int total_size_bytes = 0,
                    total_size_blocks = 0;


struct holder_sizes {
    u_int32_t  size_in_blocks;
    string name_of_file;
};


int sum(const char *fpath, const struct stat *sb, int typeflag) {
    total_size_bytes += sb->st_size;
    total_size_blocks += sb->st_blocks;
    return 0;
}


void scan_and_make_list_for_folders(const char *path)
{
               // size in bytes is used as a key
  multimap <  u_int32_t, holder_sizes> lines;

  vector <string> pathes;
  pathes.push_back(path);

  dp=opendir(pathes[0].c_str());
  if (dp==NULL) {
      perror("opendir");
      exit(EXIT_FAILURE);
  }

  while ((entry = readdir(dp))) {
    if ((strcmp(entry->d_name,".")) && (strcmp(entry->d_name,"..")) ) {

       string path;
       path =pathes[0];// избежать наклейки путей
       path+='/';
       path+=entry->d_name;

      struct stat holder;

      if ( stat(path.c_str(),&holder) == -1 ) {
          perror("stat error");
          exit(EXIT_FAILURE);
      }

      if ( S_ISDIR(holder.st_mode)) {

          if (ftw(path.c_str(), &sum, FTW_D)) {
                 perror("ftw error");
                 exit(EXIT_FAILURE);
             }

         holder_sizes d;
         d.name_of_file = path;
         d.size_in_blocks = total_size_blocks;
         lines.insert ( std::pair<u_int32_t,holder_sizes>( total_size_bytes, d) );
      }
      else {
       holder_sizes d;
       d.name_of_file = path;
       d.size_in_blocks = holder.st_blocks;
       lines.insert ( std::pair<u_int32_t, holder_sizes>( holder.st_size, d) );
      }

      total_size_bytes =0;
      total_size_blocks = 0;
  }
 }

 closedir(dp);

 std::multimap<u_int32_t, holder_sizes>::iterator it;

 for (it=lines.begin(); it!=lines.end(); ++it)
  cout<< holder_sizes ((*it).second).name_of_file << "  " <<   (*it).first<<" bytes " <<
         holder_sizes((*it).second).size_in_blocks<<" blocks \n";
}


// du -b  name - проверка размера папки в байтах
// du --block-size=512 name - проверка количества блоков
int main(int argc, char *argv[])
{
//  argc=2;
//  argv[1]="/home/sh/Documents";

    if( argc !=2) {
        cerr<<"wrong number of input parameters"<<endl;
        return EXIT_FAILURE;
    }

    // check if it's a directory
    if (  (dp=opendir(argv[1])) == NULL  ) {
        cerr<<"it's not  a directory, can't be opened\n";
       return EXIT_FAILURE;
    }

 scan_and_make_list_for_folders(argv[1]);
 return EXIT_SUCCESS;
}
